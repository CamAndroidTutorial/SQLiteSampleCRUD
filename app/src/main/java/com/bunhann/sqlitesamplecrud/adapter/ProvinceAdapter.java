package com.bunhann.sqlitesamplecrud.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bunhann.sqlitesamplecrud.R;
import com.bunhann.sqlitesamplecrud.model.Province;

import java.util.List;

public class ProvinceAdapter extends RecyclerView.Adapter<ProvinceAdapter.MyViewHolder>{


    private Context context;
    private List<Province> provinceList;
    private LayoutInflater inflater;

    public ProvinceAdapter(Context context, List<Province> provinceList) {
        this.context = context;
        this.provinceList = provinceList;
        this.inflater = LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View convertView = inflater.inflate(R.layout.province_list_item, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(convertView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Province p = provinceList.get(position);

        holder.tvNameKh.setText(p.getNameKh());
        holder.tvName.setText(p.getNameEn());

    }

    @Override
    public int getItemCount() {
        return provinceList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvNameKh;
        public TextView tvName;

        public MyViewHolder(View view) {
            super(view);
            tvNameKh = view.findViewById(R.id.tvTitleKhmer);
            tvName = view.findViewById(R.id.tvTitleEnglish);
        }
    }

}
