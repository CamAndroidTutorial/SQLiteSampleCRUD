package com.bunhann.sqlitesamplecrud.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bunhann.sqlitesamplecrud.model.Province;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "gazeetter.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // create Province table
        db.execSQL(Province.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Province.TABLE_NAME);
        // Create tables again
        onCreate(db);
    }


    //Insert Province
    public long insertProvince(Province province) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Province.COLUMN_NAME_KHMER, province.getNameKh());
        values.put(Province.COLUMN_NAME_EN, province.getNameEn());

        // insert row
        long id = db.insert(Province.TABLE_NAME, null, values);
        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public Province getProvince(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Province.TABLE_NAME,
                new String[]{Province.COLUMN_ID, Province.COLUMN_NAME_KHMER, Province.COLUMN_NAME_EN},
                Province.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare province object
        Province note = new Province(
                cursor.getInt(cursor.getColumnIndex(Province.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Province.COLUMN_NAME_KHMER)),
                cursor.getString(cursor.getColumnIndex(Province.COLUMN_NAME_EN)));

        // close the db connection
        cursor.close();

        return note;
    }

    public List<Province> getAllProvinces() {
        List<Province> provinces = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + Province.TABLE_NAME + " ORDER BY " +
                Province.COLUMN_NAME_KHMER + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Province province = new Province();
                province.setId(cursor.getInt(cursor.getColumnIndex(Province.COLUMN_ID)));
                province.setNameKh(cursor.getString(cursor.getColumnIndex(Province.COLUMN_NAME_KHMER)));
                province.setNameEn(cursor.getString(cursor.getColumnIndex(Province.COLUMN_NAME_EN)));

                provinces.add(province);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return Province list
        return provinces;
    }

    public int getProvincesCount() {
        String countQuery = "SELECT  * FROM " + Province.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public int updateProvince(Province p) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Province.COLUMN_NAME_KHMER, p.getNameKh());
        values.put(Province.COLUMN_NAME_EN, p.getNameEn());

        // updating row
        return db.update(Province.TABLE_NAME, values, Province.COLUMN_ID + " = ?",
                new String[]{String.valueOf(p.getId())});
    }

    public void deleteProvince(Province note) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Province.TABLE_NAME, Province.COLUMN_ID + " = ?",
                new String[]{String.valueOf(note.getId())});
        db.close();
    }

}
