package com.bunhann.sqlitesamplecrud.model;

public class Province {

    public static final String TABLE_NAME = "provinces";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME_KHMER = "name_kh";
    public static final String COLUMN_NAME_EN = "name_en";

    private int id;
    private String nameKh;
    private String nameEn;


    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME_KHMER + " TEXT,"
                    + COLUMN_NAME_EN + " TEXT"
                    + ")";

    public Province() {
    }

    public Province(int id, String nameKh, String nameEn) {
        this.id = id;
        this.nameKh = nameKh;
        this.nameEn = nameEn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameKh() {
        return nameKh;
    }

    public void setNameKh(String nameKh) {
        this.nameKh = nameKh;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

}
