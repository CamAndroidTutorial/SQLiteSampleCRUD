package com.bunhann.sqlitesamplecrud;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bunhann.sqlitesamplecrud.adapter.ProvinceAdapter;
import com.bunhann.sqlitesamplecrud.db.DatabaseHelper;
import com.bunhann.sqlitesamplecrud.model.Province;
import com.bunhann.sqlitesamplecrud.utils.MyDividerItemDecoration;
import com.bunhann.sqlitesamplecrud.utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {

    private ProvinceAdapter provinceAdapter;
    private List<Province> provinceList = new ArrayList<>();
    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerView;
    private TextView noData;

    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        noData = (TextView) findViewById(R.id.empty_data_view);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        db = new DatabaseHelper(this);

        provinceList.addAll(db.getAllProvinces());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProvinceDialog(false, null, -1);
            }
        });

        provinceAdapter = new ProvinceAdapter(this, provinceList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(provinceAdapter);

        toggleEmptyData();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {
                showActionsDialog(position);
            }
        }));

    }

    /**
     * Inserting new Province in db
     * and refreshing the list
     */
    private void createProvince(Province province) {
        // inserting province in db and getting
        // newly inserted province id
        long id = db.insertProvince(province);

        // get the newly inserted province from db
        Province n = db.getProvince(id);

        if (n != null) {
            // adding new province to array list at 0 position
            provinceList.add(0, n);
            // refreshing the list
            provinceAdapter.notifyDataSetChanged();

            toggleEmptyData();
        }
    }

    /**
     * Updating province in db and updating
     * item in the list by its position
     */
    private void updateProvince(Province province, int position) {
        Province n = provinceList.get(position);
        // updating province text
        n.setNameEn(province.getNameEn());
        n.setNameKh(province.getNameKh());

        // updating province in db
        db.updateProvince(n);

        // refreshing the list
        provinceList.set(position, n);
        provinceAdapter.notifyItemChanged(position);

        toggleEmptyData();
    }

    /**
     * Deleting province from SQLite and removing the
     * item from the list by its position
     */
    private void deleteProvince(int position) {
        // deleting the province from db
        db.deleteProvince(provinceList.get(position));

        // removing the province from the list
        provinceList.remove(position);
        provinceAdapter.notifyItemRemoved(position);

        toggleEmptyData();
    }


    /**
     * Opens dialog with Edit - Delete options
     * Edit - 0
     * Delete - 0
     */
    private void showActionsDialog(final int position) {
        CharSequence colors[] = new CharSequence[]{"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    showProvinceDialog(true, provinceList.get(position), position);
                } else {
                    deleteProvince(position);
                }
            }
        });
        builder.show();
    }


    /**
     * Shows alert dialog with EditText options to enter / edit
     * a province.
     * when shouldUpdate=true, it automatically displays old province and changes the
     * button text to UPDATE
     */
    private void showProvinceDialog(final boolean shouldUpdate, final Province province, final int position) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
        View view = layoutInflaterAndroid.inflate(R.layout.province_dialog, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);


        final EditText inputProvinceKh = (EditText) view.findViewById(R.id.edNameKh);
        final EditText inputProvince = (EditText) view.findViewById(R.id.edName);
        TextView dialogTitle = (TextView) view.findViewById(R.id.dialog_title);

        alertDialogBuilderUserInput.setView(view);

        dialogTitle.setText(!shouldUpdate ? getString(R.string.lbl_new_province_title) : getString(R.string.lbl_edit_province_title));

        if (shouldUpdate && province != null) {
            inputProvinceKh.setText(province.getNameKh());
            inputProvince.setText(province.getNameEn());
        }
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(shouldUpdate ? "update" : "save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show toast message when no text is entered
                if (TextUtils.isEmpty(inputProvinceKh.getText().toString())) {
                    Toast.makeText(Main2Activity.this, "Enter province!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    alertDialog.dismiss();
                }

                Province pro = new Province();
                pro.setNameEn(inputProvince.getText().toString());
                pro.setNameKh(inputProvinceKh.getText().toString());
                // check if user updating province
                if (shouldUpdate && province != null) {
                    // update province by it's id
                    updateProvince(pro, position);
                } else {
                    // create new province
                    createProvince(pro);
                }
            }
        });
    }

    /**
     * Toggling list and empty province view
     */
    private void toggleEmptyData() {
        // you can check provinceList.size() > 0
        if (db.getProvincesCount() > 0) {
            noData.setVisibility(View.GONE);
        } else {
            noData.setVisibility(View.VISIBLE);
        }
    }

}
